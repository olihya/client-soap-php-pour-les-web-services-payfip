# Client SOAP PHP pour les web-services Payfip

Client SOAP PHP pour pouvoir utiliser les web-services Payfip.
Payfip offre un service de paiement en ligne aux usagers des services des administrations de l'état ou des collectivités locales.
Ce client à été réalisé pour des établissements de l'enseignement agricole mais peut être utilisé par toute administration voulant proposer le paiement en ligne.
L'objectif est d'aider les administrations qui n'ont pas les moyens ou les compétences pour apporter ce service aux usagers.
Mais aussi de faire du partage et de travailler pour le bien commun.
Si vous avez besoin d'aide pour la mise en place, vous pouvez me contacter à olivier.puygranier[@]educagri.fr.
Penser à  enlever les [] autour du @ pour que l'adresse mail fonctionne.

Client SOAP PHP pour les web-services Payfip © 2019 by Olivier Puygranier is licensed under Attribution-NonCommercial-ShareAlike 4.0 International
