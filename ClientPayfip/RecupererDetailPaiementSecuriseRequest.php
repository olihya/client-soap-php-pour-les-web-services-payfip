<?php

class RecupererDetailPaiementSecuriseRequest
{

    /**
     * @var string $idOp
     */
    protected $idOp = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getIdOp()
    {
      return $this->idOp;
    }

    /**
     * @param string $idOp
     * @return RecupererDetailPaiementSecuriseRequest
     */
    public function setIdOp($idOp)
    {
      $this->idOp = $idOp;
      return $this;
    }

}
