<?php

class recupererDetailPaiementSecuriseResponse
{

    /**
     * @var RecupererDetailPaiementSecuriseResponse $return
     */
    protected $return = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return RecupererDetailPaiementSecuriseResponse
     */
    public function getReturn()
    {
      return $this->return;
    }

    /**
     * @param RecupererDetailPaiementSecuriseResponse $return
     * @return recupererDetailPaiementSecuriseResponse
     */
    public function setReturn($return)
    {
      $this->return = $return;
      return $this;
    }

}
