<?php

class PaiementSecuriseService extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
  'CreerPaiementSecuriseRequest' => '\\CreerPaiementSecuriseRequest',
  'CreerPaiementSecuriseResponse' => '\\CreerPaiementSecuriseResponse',
  'RecupererDetailClientRequest' => '\\RecupererDetailClientRequest',
  'RecupererDetailClientResponse' => '\\RecupererDetailClientResponse',
  'RecupererDetailPaiementSecuriseRequest' => '\\RecupererDetailPaiementSecuriseRequest',
  'RecupererDetailPaiementSecuriseResponse' => '\\RecupererDetailPaiementSecuriseResponse',
  'creerPaiementSecurise' => '\\creerPaiementSecurise',
  'creerPaiementSecuriseResponse' => '\\creerPaiementSecuriseResponse',
  'FonctionnelleErreur' => '\\FonctionnelleErreur',
  'TechDysfonctionnementErreur' => '\\TechDysfonctionnementErreur',
  'TechIndisponibiliteErreur' => '\\TechIndisponibiliteErreur',
  'TechProtocolaireErreur' => '\\TechProtocolaireErreur',
  'recupererDetailClient' => '\\recupererDetailClient',
  'recupererDetailClientResponse' => '\\recupererDetailClientResponse',
  'recupererDetailPaiementSecurise' => '\\recupererDetailPaiementSecurise',
  'recupererDetailPaiementSecuriseResponse' => '\\recupererDetailPaiementSecuriseResponse',
);

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = null)
    {
    
  foreach (self::$classmap as $key => $value) {
    if (!isset($options['classmap'][$key])) {
      $options['classmap'][$key] = $value;
    }
  }
      $options = array_merge(array (
  'features' => 1,
), $options);
      if (!$wsdl) {
        $wsdl = '/PaiementSecuriseService.wsdl';
      }
      parent::__construct($wsdl, $options);
    }

    /**
     * @param creerPaiementSecurise $parameters
     * @return creerPaiementSecuriseResponse
     */
    public function creerPaiementSecurise(creerPaiementSecurise $parameters)
    {
      return $this->__soapCall('creerPaiementSecurise', array($parameters));
    }

    /**
     * @param recupererDetailPaiementSecurise $parameters
     * @return recupererDetailPaiementSecuriseResponse
     */
    public function recupererDetailPaiementSecurise(recupererDetailPaiementSecurise $parameters)
    {
      return $this->__soapCall('recupererDetailPaiementSecurise', array($parameters));
    }

    /**
     * @param recupererDetailClient $parameters
     * @return recupererDetailClientResponse
     */
    public function recupererDetailClient(recupererDetailClient $parameters)
    {
      return $this->__soapCall('recupererDetailClient', array($parameters));
    }

}
