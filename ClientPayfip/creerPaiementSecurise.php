<?php

class creerPaiementSecurise
{

    /**
     * @var CreerPaiementSecuriseRequest $arg0
     */
    protected $arg0 = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return CreerPaiementSecuriseRequest
     */
    public function getArg0()
    {
      return $this->arg0;
    }

    /**
     * @param CreerPaiementSecuriseRequest $arg0
     * @return creerPaiementSecurise
     */
    public function setArg0($arg0)
    {
      $this->arg0 = $arg0;
      return $this;
    }

}
