<?php

class recupererDetailPaiementSecurise
{

    /**
     * @var RecupererDetailPaiementSecuriseRequest $arg0
     */
    protected $arg0 = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return RecupererDetailPaiementSecuriseRequest
     */
    public function getArg0()
    {
      return $this->arg0;
    }

    /**
     * @param RecupererDetailPaiementSecuriseRequest $arg0
     * @return recupererDetailPaiementSecurise
     */
    public function setArg0($arg0)
    {
      $this->arg0 = $arg0;
      return $this;
    }

}
