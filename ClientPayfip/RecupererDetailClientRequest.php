<?php

class RecupererDetailClientRequest
{

    /**
     * @var string $numCli
     */
    protected $numCli = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getNumCli()
    {
      return $this->numCli;
    }

    /**
     * @param string $numCli
     * @return RecupererDetailClientRequest
     */
    public function setNumCli($numCli)
    {
      $this->numCli = $numCli;
      return $this;
    }

}
