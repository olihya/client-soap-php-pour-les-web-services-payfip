<?php

class recupererDetailClientResponse
{

    /**
     * @var RecupererDetailClientResponse $return
     */
    protected $return = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return RecupererDetailClientResponse
     */
    public function getReturn()
    {
      return $this->return;
    }

    /**
     * @param RecupererDetailClientResponse $return
     * @return recupererDetailClientResponse
     */
    public function setReturn($return)
    {
      $this->return = $return;
      return $this;
    }

}
