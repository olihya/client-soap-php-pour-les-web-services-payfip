<?php

ob_start();

if(!empty($_POST['website'])){

	header('Location: https://google.com');

	die();

}

//Rapport d'erreurs variables

ini_set('display_errors', 'Off');

error_reporting(E_ALL);   // Activer le rapport d'erreurs PHP . Vous pouvez n'utiliser que cette ligne, elle donnera déjà beaucoup de détails.



$variables = get_defined_vars(); 
// Donne le contenu et les valeurs de toutes les variables dans la portée actuelle

$var_ignore=array("GLOBALS", "_ENV", "_SERVER"); // Détermine les var à ne pas afficher

echo ("<strong>Etat des variables a la ligne : ".__LINE__." dans le fichier : ".__FILE__."</strong><br />\n");

$nom_fonction=__FUNCTION__;

if (isset($nom_fonction)&&$nom_fonction!="") {

  echo ("<strong>Dans la fonction : ".$nom_fonction."</strong><br />\n");

}

foreach ($variables as $key=>$value) {

  if (!in_array($key, $var_ignore)&&strpos($key,"HTTP")===false) {

    echo "<pre>";

    echo ("$".$key." => ");

    print_r($value);

    echo "</pre>\n";

  }

} 



 //Recaptcha Google (facultative) a personnaliser selon vos besoins

// Verifie si le formulaire a été envoyé:

if (isset($_POST['g-recaptcha-response'])) {

    $captcha = $_POST['g-recaptcha-response'];

} else {

    $captcha = false;

}



if (!$captcha) {

    //Do something with error

	echo "Erreur Captcha";

} else {

    $secret   = 'secretkey';

    $response = file_get_contents(

        "https://www.google.com/recaptcha/api/siteverify?secret=" . $secret . "&response=" . $captcha . "&remoteip=" . $_SERVER['REMOTE_ADDR']

    );

    // use json_decode to extract json response

    $response = json_decode($response);



    if ($response->success === false) {

        //Do something with error

		echo "Probleme Captcha";

    }

}



//... The Captcha is valid you can continue with the rest of your code

//... Add code to filter access using $response . score

if ($response->success==true && $response->score <= 0.5) {

    //Do something to denied access

	header("Location:https://google.com");

die();}

require_once __DIR__ . '/autoload.php';

$year = date("Y");
$numeroClientPayfip = "030068";
$paymentType = "T";
$urlnot = "https://tonsite.com";
$urlredir = "https://tonsite.com";
$mel = $_POST['email'];
$nom = $_POST['nom'];
$prenom = $_POST['prenom'];
$telephone = $_POST['telephone'];
$amount = $_POST['amount'];
$cents = bcmul($amount, 100);
$numFact = $_POST['numFact'];
$objet = $_POST['objet'];



$parameters["arg0"]['exer'] = $year;
$parameters["arg0"]['mel'] = $mel;
$parameters["arg0"]['montant'] = $cents;
$parameters["arg0"]['numcli'] = $numeroClientPayfip;
$parameters["arg0"]['objet'] = $objet;
$parameters["arg0"]['refdet'] = $numFact;
$parameters["arg0"]['saisie'] = $paymentType;
$parameters["arg0"]['urlnotif'] = $urlnot;
$parameters["arg0"]['urlredirect'] = $urlredir;



	$client = new SoapClient("PaiementSecuriseService.wsdl", 
	array(	'exceptions' => 0,
			'soap_version' => SOAP_1_1,
			'trace' => 0,
			'use' => SOAP_LITERAL,
			'compression' => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP,
			'cache_wsdl'=>WSDL_CACHE_NONE
	));
	
	
	$cps = $client->creerPaiementSecurise($parameters);
	

	$cps->creerPaiementSecuriseResponse->return;
//  var_dump($cps);
	$idOp = $cps->return->idOp;

header("Location:https://www.payfip.gouv.fr/tpa/paiementws.web?idop=".$idOp);

//commandes pour le débogage:
// echo "<pre>",'LAST REQUEST: ' . $client->__getLastRequest(), "</pre>";
// echo "<pre>",'LAST RESPONSE: ' . $client->__getLastResponse(), "</pre>";
// echo "<pre>",'LAST REQUEST HEADERS: ' . $client->__getLastRequestHeaders(), "</pre>";
// echo "<pre>",'LAST RESPONSE HEADERS: ' . $client->__getLastResponseHeaders(), "</pre>";
// var_dump($idOp);

//envoyer un mail à la compta ou toute personne gérant les paiements

$to_email = 'emailcompta@educagri.fr';
$subject = 'Nouveau paiement';
$message = "Paiement de: ". $prenom." ". $nom. "\r\n";
$message .= "Telephone: ". $telephone. "\r\n";
$message .= "Email: ". $mel. "\r\n";
$message .= "N° de facture: ". $numFact. "\r\n";
$message .= "Objet: ". $objet. "\r\n";
$message .= "Montant: ". $amount. "\r\n";
$headers = 'From: webmaster@tonsite.com';
mail($to_email,$subject,$message,$headers);	

?>
