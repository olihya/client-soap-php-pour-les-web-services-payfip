<?php

class FonctionnelleErreur
{

    /**
     * @var string $code
     */
    protected $code = null;

    /**
     * @var string $descriptif
     */
    protected $descriptif = null;

    /**
     * @var string $libelle
     */
    protected $libelle = null;

    /**
     * @var string $message
     */
    protected $message = null;

    /**
     * @var int $severite
     */
    protected $severite = null;

    /**
     * @param int $severite
     */
    public function __construct($severite)
    {
      $this->severite = $severite;
    }

    /**
     * @return string
     */
    public function getCode()
    {
      return $this->code;
    }

    /**
     * @param string $code
     * @return FonctionnelleErreur
     */
    public function setCode($code)
    {
      $this->code = $code;
      return $this;
    }

    /**
     * @return string
     */
    public function getDescriptif()
    {
      return $this->descriptif;
    }

    /**
     * @param string $descriptif
     * @return FonctionnelleErreur
     */
    public function setDescriptif($descriptif)
    {
      $this->descriptif = $descriptif;
      return $this;
    }

    /**
     * @return string
     */
    public function getLibelle()
    {
      return $this->libelle;
    }

    /**
     * @param string $libelle
     * @return FonctionnelleErreur
     */
    public function setLibelle($libelle)
    {
      $this->libelle = $libelle;
      return $this;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
      return $this->message;
    }

    /**
     * @param string $message
     * @return FonctionnelleErreur
     */
    public function setMessage($message)
    {
      $this->message = $message;
      return $this;
    }

    /**
     * @return int
     */
    public function getSeverite()
    {
      return $this->severite;
    }

    /**
     * @param int $severite
     * @return FonctionnelleErreur
     */
    public function setSeverite($severite)
    {
      $this->severite = $severite;
      return $this;
    }

}
