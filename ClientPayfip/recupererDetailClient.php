<?php

class recupererDetailClient
{

    /**
     * @var RecupererDetailClientRequest $arg0
     */
    protected $arg0 = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return RecupererDetailClientRequest
     */
    public function getArg0()
    {
      return $this->arg0;
    }

    /**
     * @param RecupererDetailClientRequest $arg0
     * @return recupererDetailClient
     */
    public function setArg0($arg0)
    {
      $this->arg0 = $arg0;
      return $this;
    }

}
