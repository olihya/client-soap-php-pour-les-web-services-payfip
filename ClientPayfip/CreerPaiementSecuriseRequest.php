<?php

date_default_timezone_set('Europe/Paris');


class CreerPaiementSecuriseRequest
{

    /**
     * @var string $exer
     */
    protected $exer = date("Y");

    /**
     * @var string $mel
     */
    protected $mel = null;

    /**
     * @var string $montant
     */
    protected $montant = null;

    /**
     * @var string $numcli
     */
    protected $numcli = null;

    /**
     * @var string $objet
     */
    protected $objet = null;

    /**
     * @var string $refdet
     */
    protected $refdet = null;

    /**
     * @var string $saisie
     */
    protected $saisie = null;

    /**
     * @var string $urlnotif
     */
    protected $urlnotif = null;

    /**
     * @var string $urlredirect
     */
    protected $urlredirect = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getExer()
    {
      return $this->exer;
    }

    /**
     * @param string $exer
     * @return CreerPaiementSecuriseRequest
     */
    public function setExer($exer)
    {
      $this->exer = $exer;
      return $this;
    }

    /**
     * @return string
     */
    public function getMel()
    {
      return $this->mel;
    }

    /**
     * @param string $mel
     * @return CreerPaiementSecuriseRequest
     */
    public function setMel($mel)
    {
      $this->mel = $mel;
      return $this;
    }

    /**
     * @return string
     */
    public function getMontant()
    {
      return $this->montant;
    }

    /**
     * @param string $montant
     * @return CreerPaiementSecuriseRequest
     */
    public function setMontant($montant)
    {
      $this->montant = $montant;
      return $this;
    }

    /**
     * @return string
     */
    public function getNumcli()
    {
      return $this->numcli;
    }

    /**
     * @param string $numcli
     * @return CreerPaiementSecuriseRequest
     */
    public function setNumcli($numcli)
    {
      $this->numcli = $numcli;
      return $this;
    }

    /**
     * @return string
     */
    public function getObjet()
    {
      return $this->objet;
    }

    /**
     * @param string $objet
     * @return CreerPaiementSecuriseRequest
     */
    public function setObjet($objet)
    {
      $this->objet = $objet;
      return $this;
    }

    /**
     * @return string
     */
    public function getRefdet()
    {
      return $this->refdet;
    }

    /**
     * @param string $refdet
     * @return CreerPaiementSecuriseRequest
     */
    public function setRefdet($refdet)
    {
      $this->refdet = $refdet;
      return $this;
    }

    /**
     * @return string
     */
    public function getSaisie()
    {
      return $this->saisie;
    }

    /**
     * @param string $saisie
     * @return CreerPaiementSecuriseRequest
     */
    public function setSaisie($saisie)
    {
      $this->saisie = $saisie;
      return $this;
    }

    /**
     * @return string
     */
    public function getUrlnotif()
    {
      return $this->urlnotif;
    }

    /**
     * @param string $urlnotif
     * @return CreerPaiementSecuriseRequest
     */
    public function setUrlnotif($urlnotif)
    {
      $this->urlnotif = $urlnotif;
      return $this;
    }

    /**
     * @return string
     */
    public function getUrlredirect()
    {
      return $this->urlredirect;
    }

    /**
     * @param string $urlredirect
     * @return CreerPaiementSecuriseRequest
     */
    public function setUrlredirect($urlredirect)
    {
      $this->urlredirect = $urlredirect;
      return $this;
    }

}
