<?php

class creerPaiementSecuriseResponse
{

    /**
     * @var CreerPaiementSecuriseResponse $return
     */
    protected $return = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return CreerPaiementSecuriseResponse
     */
    public function getReturn()
    {
      return $this->return;
    }

    /**
     * @param CreerPaiementSecuriseResponse $return
     * @return creerPaiementSecuriseResponse
     */
    public function setReturn($return)
    {
      $this->return = $return;
      return $this;
    }

}
