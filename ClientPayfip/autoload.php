<?php


 function autoload_f04aebfce606e444485fdb1bd0cc43b8($class)
{
    $classes = array(
        'PaiementSecuriseService' => __DIR__ .'/PaiementSecuriseService.php',
        'CreerPaiementSecuriseRequest' => __DIR__ .'/CreerPaiementSecuriseRequest.php',
        'CreerPaiementSecuriseResponse' => __DIR__ .'/CreerPaiementSecuriseResponse.php',
        'RecupererDetailClientRequest' => __DIR__ .'/RecupererDetailClientRequest.php',
        'RecupererDetailClientResponse' => __DIR__ .'/RecupererDetailClientResponse.php',
        'RecupererDetailPaiementSecuriseRequest' => __DIR__ .'/RecupererDetailPaiementSecuriseRequest.php',
        'RecupererDetailPaiementSecuriseResponse' => __DIR__ .'/RecupererDetailPaiementSecuriseResponse.php',
        'creerPaiementSecurise' => __DIR__ .'/creerPaiementSecurise.php',
        'creerPaiementSecuriseResponse' => __DIR__ .'/creerPaiementSecuriseResponse.php',
        'FonctionnelleErreur' => __DIR__ .'/FonctionnelleErreur.php',
        'TechDysfonctionnementErreur' => __DIR__ .'/TechDysfonctionnementErreur.php',
        'TechIndisponibiliteErreur' => __DIR__ .'/TechIndisponibiliteErreur.php',
        'TechProtocolaireErreur' => __DIR__ .'/TechProtocolaireErreur.php',
        'recupererDetailClient' => __DIR__ .'/recupererDetailClient.php',
        'recupererDetailClientResponse' => __DIR__ .'/recupererDetailClientResponse.php',
        'recupererDetailPaiementSecurise' => __DIR__ .'/recupererDetailPaiementSecurise.php',
        'recupererDetailPaiementSecuriseResponse' => __DIR__ .'/recupererDetailPaiementSecuriseResponse.php'
    );
    if (!empty($classes[$class])) {
        include $classes[$class];
    };
}

spl_autoload_register('autoload_f04aebfce606e444485fdb1bd0cc43b8');

// Do nothing. The rest is just leftovers from the code generation.
{
}
